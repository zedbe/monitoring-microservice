# Monitoring microservice
Used technologies: node.js, restify, mysql, docker

### Dependencies:
 - npm>5 ... because of *lock.json file
 - Mysql>5.5.53 ... requires '/var/lib/mysql-files' instead of '/var/lib/mysql'! This can reproduce fail building image.

 
## How to use:
 1. Make docker images. You can use Makefile: `make build` which should create 2 images of nodejs microservice and mysql db.
 2. Run containers via `docker-compose up -d microservice` which will also run dockerized mysql in other container.
 3. If everything was OK and running correctly. You can jump into browser addons like Postman in Chrome or Rested in FF and others (or use curl :)).
 4. Now you can create REST querying the nodejs microservice. Root page is ***http://localhost:1992***. Requests table below:
 5. *Right now, monitoring is 95% function only with Users manipulating*
 
| Endpoint    | Method | Description               |
| --------    | ------ | -----------               |
| /endpoints  | GET    |  list endpoints of authorized user           |
| /results    | GET    |  list last 10 results of authorized user           |
| /users      | GET    |  list all users           |
| /users/{id} | GET    |  list single user by ID   |
| /users      | POST   |  create an user           |
| /users/{id} | PUT    |  update an user by ID     |
| /users/{id} | DELETE |  delete single user by ID |

TODOS:
1) handling of more common endpoints and catch all other unspported routes
2) refactor code to make it more beautiful (object usage)
3) tests

### Shortcuts:
 - insert this: `alias mysql-monitor='mysql -uroot -proot -h0.0.0.0 monitor_db'` into .bashrc to directly connect into DB
 
#### Obstacles:
 - It's possible that when MySQL container is runnig ok and Node server too, there can be a problem with compatibilities between 
 MySql Server (v8) and client module in Nodejs (v2.16). Solution is to connect to running DB and run this:
	```sql
	 use mysql;
	 update user set authentication_string=NULL, plugin='mysql_native_password' where user='root';
	 flush privileges;		-- in 99% needed
	 ```
 - Other problem can scream during building DB docker at 9/20 step, just repeat building.
 - For most basic execution, when just everything is wrong with version/dependencies etc.
 	1. you must build docker with DB OR run schema.sql on your own mysql DB (but then you have to wire Node to DB)
  	2. Node is easily runnable without docker, you just need npm.
  		- `cd microservice`
  		- `npm install`
  		- `node server.js`
	3. Then jump to How to use step 3 :)
 - Take your time when start mysql in docker. DB needs time to be accessible from outside :) 
