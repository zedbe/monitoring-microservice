// This module will obtain DB queries

module.exports = {
    //USERS
    createUser: 'INSERT INTO Users (`name`, `email`) VALUES (?, ?)',
    editUser: 'UPDATE `Users` SET `name`=?, `email`=? where `user_id`=?',
    delUser: 'DELETE FROM `Users` WHERE `user_id`=?',
    listUsers: 'select * from Users',
    listUser: 'select * from Users where user_id=?',

    //AUTH
    tokenAuth: 'select * from Users where access_token=?',

    //ENDPOINTS table
    putEndpointAuth: 'INSERT INTO Endpoints (`name`, `url`, `user_id`) VALUES (?,?,?)',
    putEndpointUnauth: 'INSERT INTO Endpoints (`name`, `url`) VALUES (?,?)',
    testAuthEndpoint: 'select * from Endpoints where `name`=? and `url`=? and `user_id`=?',
    testUnauthEndpoint: 'select * from Endpoints where `name`=? and `url`=? and `user_id` IS NULL',
    newCheckDate: 'UPDATE `Endpoints` SET `last_check_date`=CURRENT_TIMESTAMP where `endpoint_id`=?',
    updateInterval: 'UPDATE `Endpoints` SET `monitor_interval`=TIME_TO_SEC(TIMEDIFF(last_check_date, create_date)) where `endpoint_id`=?',

    //Result table
    pushResult: 'INSERT INTO Results (`status_code`, `return_payload`, `endpoint_id`) VALUES (?, ?, ?)',

    //Show endpoints/results table
    listEndpoints: 'select Users.user_id as user_ID, Users.name as user, Endpoints.name as name, Endpoints.url as url, ' +
        'Endpoints.create_date as created, Endpoints.last_check_date as last_check, ' +
        'Endpoints.monitor_interval as interval_sec ' +
        'from Endpoints join Users on Users.access_token = ? order by created',
    listTenResults: 'select date_check,  status_code, Endpoints.name as endpoint_and_method, ' +
        'return_payload from Results join Endpoints on Results.endpoint_id = Endpoints.endpoint_id ' +
        'join Users on Users.access_token = "?" order by date_check limit 10;',
};
