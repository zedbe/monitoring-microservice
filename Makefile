all: rmi-all rm-all

build-node:
	$(MAKE) -w -C microservice build

build-db:
	$(MAKE) -w -C mysql build

rm-all:
	docker rm $(docker ps -a -q)

rmi-all:
	docker rmi $(docker images -q)