let mysql = require('mysql');

module.exports = {
    name: 'rest-api',
    hostname : '0.0.0.0',
    port: 1992,
    version: '1.0.0',
    db: {
        get : mysql.createConnection({
            host     : '0.0.0.0',
            user     : 'root',
            // password : 'root',
            database : 'monitor_db',
            port     : 3306,
        })
    }
};