/*
% Description: Main file which starts nodejs server, for monitoring rest methods (over mysql).
% Author: Zdenko Brandejs
*/

// Tech modules
const restify = require('restify');
const mysql = require('mysql');

// Own modules
const config = require('./lib/config');
const sql = require('./lib/sql');

// Set DB connection; authorization flag
let connection = config.db.get;
let authFlag;
let status_code;
let message = {};
let token;

// Initialize server
const server = restify.createServer({
    name    : config.name,
    version : config.version,
    url : config.hostname
});

// Setup restify plugins
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser({
    mapParams: true
}));

///////////////////////////////////////// USERS ////////////////////////////////////////////////
// List all users
server.get('/users', function (req, res) {
    if (authFlag === true) {
        connection.query(sql.listUsers, function (error, result) {
            if (error) throw error;
            else {
                message['message'] = 'List of all users.';
                message['result'] = result;
                res.send(status_code, message);
            }
        });
    } else {
        console.log("[" + req._date + "] @@@ " + message.message + " ACCESS DENIED!!!");
        res.send(status_code, message);
    }
});

// List single user by his ID
server.get('/users/:id', function (req, res) {
    if (authFlag === true) {
        connection.query(sql.listUser, [req.params.id], function (error, result) {
            if (error) throw error;
            else {
                message['message'] = 'Show user.';
                delete result['access_token'];
                message['result'] = result;
                res.send(status_code, message);
            }
       });
    } else {
        console.log("[" + req._date + "] @@@ " + message.message + " ACCESS DENIED!!!");
        res.send(status_code, message);
    }
});

// Create a new user
server.post('/users', function (req, res) {
    if (authFlag === true) {
        connection.query(sql.createUser, [req.body.name, req.body.email],
            function (error, result) {
                if (error) {
                    throw error;
                }
                else {
                    message['message'] = 'Creating new user.';
                    message['result'] = result;
                    res.send(status_code, message);
                }
            });
    } else {
        console.log("[" + req._date + "] @@@ " + message.message + " ACCESS DENIED!!!");
        res.send(status_code, message);
    }
});

// Update user record by his ID
server.put('/users', function (req, res) {
    if (authFlag === true) {
        // would be nice TODO fix SQL, when use only one modification insted of 2
        connection.query(sql.editUser, [req.body.name, req.body.email, req.body.id],
            function (error, result) {
                if (error) throw error;
                else {
                    message['message'] = 'Updating a user.';
                    message['result'] = result;
                    res.send(status_code, message);
                }
            });
    } else {
        console.log("[" + req._date + "] @@@ " + message.message + " ACCESS DENIED!!!");
        res.send(status_code, message);
    }
});

// Delete user by his ID
server.del('/users/:id', function (req, res) {
    if (authFlag === true) {
        connection.query(sql.delUser, [req.params.id], function (error, result) {
            if (error) throw error;
            else {
                message['message'] = 'Deleting a user.';
                message['result'] = result;
                res.send(status_code, message);
            }
        });
    } else {
        console.log("[" + req._date + "] @@@ " + message.message + " ACCESS DENIED!!!");
        res.send(status_code, message);
    }
});

////////////////////////////// !!! CORE ENDPOINTS !!! /////////////////////////////////
// this will show all endpoints of (un)authorized user
server.get('/endpoints', function (req, res) {
    if (authFlag === true) {
        connection.query(sql.listEndpoints, [token], function (error, result) {
            if (error) throw error;
            else {
                message['message'] = "List of user's endpoints.";
                message['result'] = result;
                res.send(status_code, message);
            }
        });
    } else {
        console.log("[" + req._date + "] @@@ " + message.message + " ACCESS DENIED!!!");
        res.send(status_code, message);
    }
});

server.get('/results', function (req, res) {
    // this will show last 10 ressults of (un)authorized user
    if (authFlag === true) {
        connection.query(sql.listTenResults, [token], function (error, result) {
            if (error) throw error;
            else {
                message['message'] = "List of user's last 10 results.";
                message['result'] = result;
                res.send(status_code, message);
            }
        });
    } else {
        console.log("[" + req._date + "] @@@ " + message.message + " ACCESS DENIED!!!");
        res.send(status_code, message);
    }
});



/////////////////////////////// END USERS ///////////////////////////////////////

server.listen(config.port, config.hostname, () => {
    console.log('Monitoring server running at ' + server.url);
});

server.use((req, res, next) => {
    res.header('content-type', 'json');
    token = req.header('acc_token');
    let name = req.method + ":" + req.url;
    let url = server.url + req.url;

    //mostly for debugging purposes
    console.log("[" + req._date + "] @@@ " +
        "[ENDPOINT]: " + req.url +
        ", [METHOD]: " + req.method +
        ", [QUERY]: " + JSON.stringify(req.query) +
        ", [BODY]: " + JSON.stringify(req.body) +
        ", [TOKEN]: " + token
    );
    // Validation part
    // TODO filter if user wants supported endpoint else 404 and skip the rest of authorization etc...

    // Authorization part
    if (token){
        connection.query(sql.tokenAuth, token,
        function (error, result) {
            if (error) {
                throw error;
            }
            /////////////////////// Authorized USER //////////////////////////////////////////////
            else if (result[0]) {
                authFlag = true;
                status_code = 200;
                message = {
                    success: true,
                };

                let user_id = result[0].user_id;

                // test if new endpoint is really new
                connection.query(sql.testAuthEndpoint, [name, url, user_id],function (error, result) {
                    if (error) throw error;
                    else if (result[0]) {
                        let endpoint_id = result[0].endpoint_id;

                        console.log("ENDPOINT updating!");

                        // Actualize last_check_date
                        connection.query(sql.newCheckDate, [result[0].endpoint_id],function (error, result) {
                            if (error) throw error;
                            else if (result[0]) {
                                console.log(JSON.stringify(result[0]));
                            }
                        });

                        // Update interval
                        connection.query(sql.updateInterval, [result[0].endpoint_id],function (error, result) {
                            if (error) throw error;
                            else if (result[0]) {
                                console.log(JSON.stringify(result[0]));
                            }
                        });

                        // Insert result into DB
                        connection.query(sql.pushResult, [status_code, JSON.stringify(message).slice(0,255), endpoint_id],
                        function (error, result) {
                            if (error) throw error;
                            else console.log('NEW record in Results.');
                        });
                    }
                    else {
                        // This is brand new endpoint :)
                        connection.query(sql.putEndpointAuth, [name, url, user_id],function (error, result) {
                            if (error) throw error;
                            else {
                                console.log('NEW ENDPOINT set!!');

                                // Insert result into DB
                                connection.query(sql.pushResult, [status_code, JSON.stringify(message).slice(0,255), result.insertId],
                                function (error, result) {
                                    if (error) throw error;
                                    else console.log('NEW record in Results.');
                                });
                            }
                        });
                    }
                });
            }
            else {
                ///////////////////// Wrong token -> 403 ////////////////////////////////////////
                // (failed to authorize, user's input token is not assigned to any user in DB)
                authFlag = false;
                status_code = 403;
                message = {
                    success: false,
                    message: 'Wrong token!'
                };

                // I want log all endpoints, even unauthorized users
                // test if new endpoint is really new
                connection.query(sql.testUnauthEndpoint, [name, url],function (error, result) {
                    if (error) throw error;
                    else if (result[0]) {
                        let endpoint_id = result[0].endpoint_id;
                        console.log("ENDPOINT updating!");

                        // Actualize last_check_date
                        connection.query(sql.newCheckDate, [result[0].endpoint_id],function (error, result) {
                            if (error) throw error;
                            else if (result[0]) {
                                console.log(JSON.stringify(result[0]));
                            }
                        });

                        // Update interval
                        connection.query(sql.updateInterval, [result[0].endpoint_id],function (error, result) {
                            if (error) throw error;
                            else if (result[0]) {
                                console.log(JSON.stringify(result[0]));
                            }
                        });

                        // Insert result into DB
                        connection.query(sql.pushResult, [status_code, JSON.stringify(message).slice(0,255), endpoint_id],
                            function (error, result) {
                                if (error) throw error;
                                else console.log('NEW record in Results.');
                            });
                    }
                    else {
                        // This is brand new endpoint :)
                        connection.query(sql.putEndpointUnauth, [name, url],function (error, result) {
                            if (error) throw error;
                            else {
                                console.log('NEW ENDPOINT set!!');

                                // Insert result into DB
                                connection.query(sql.pushResult, [status_code, JSON.stringify(message).slice(0,255), result.insertId],
                                    function (error, result) {
                                        if (error) throw error;
                                        else console.log('NEW record in Results.');
                                    });
                            }
                        });
                    }
                });
            }
        });
    } else {
        /////////////////////////// User DIDN'T PUT any token -> 403 //////////////////////////////
        authFlag = false;
        status_code = 403;
        message = {
            success: false,
            message: 'Missing token!'
        };

        // I want log all endpoints, even unauthorized users
        // test if new endpoint is really new
        connection.query(sql.testUnauthEndpoint, [name, url],function (error, result) {
            if (error) throw error;
            else if (result[0]) {
                let endpoint_id = result[0].endpoint_id;

                console.log("ENDPOINT updating!");

                // Actualize last_check_date
                connection.query(sql.newCheckDate, [result[0].endpoint_id],function (error, result) {
                    if (error) throw error;
                    else if (result[0]) {
                        console.log(JSON.stringify(result[0]));
                    }
                });

                // Update interval
                connection.query(sql.updateInterval, [result[0].endpoint_id],function (error, result) {
                    if (error) throw error;
                    else if (result[0]) {
                        console.log(JSON.stringify(result[0]));
                    }
                });

                // Insert result into DB
                connection.query(sql.pushResult, [status_code, JSON.stringify(message).slice(0,255), endpoint_id],
                    function (error, result) {
                        if (error) throw error;
                        else console.log('NEW record in Results.');
                    });
            }
            else {
                // This is brand new endpoint :)
                connection.query(sql.putEndpointUnauth, [name, url],function (error, result) {
                    if (error) throw error;
                    else {
                        console.log('NEW ENDPOINT set!!');

                        // Insert result into DB
                        connection.query(sql.pushResult, [status_code, JSON.stringify(message).slice(0,255), result.insertId],
                            function (error, result) {
                                if (error) throw error;
                                else console.log('NEW record in Results.');
                            });
                    }
                });
            }
        });
    }
    return next();
});
