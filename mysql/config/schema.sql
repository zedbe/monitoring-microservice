CREATE TABLE IF NOT EXISTS Users(
  user_id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  access_token VARCHAR(36) NOT NULL, -- normally I would use BINARY(16) with generating via unhex(replace(uuid(),'-',''))
  PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS Endpoints(
  endpoint_id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) DEFAULT NULL,
  url VARCHAR(255) DEFAULT NULL,
  create_date DATETIME DEFAULT NULL,
  last_check_date DATETIME DEFAULT NULL,
  monitor_interval INT(100) DEFAULT 0,
  user_id INT(11) DEFAULT NULL,
  PRIMARY KEY (endpoint_id),
  FOREIGN KEY (user_id) REFERENCES Users(user_id)
);

CREATE TABLE IF NOT EXISTS Results(
  result_id INT(11) NOT NULL AUTO_INCREMENT,
  date_check DATETIME NOT NULL,
  status_code INT(4) NOT NULL,
  return_payload VARCHAR(255) NOT NULL,
  endpoint_id INT(11) NOT NULL,
  PRIMARY KEY (result_id),
  FOREIGN KEY (endpoint_id) REFERENCES Endpoints(endpoint_id)
);

-- TESTING USERS
-- Jablotron 93f39e2f-80de-4033-99ee-249d92736a25
insert into Users(name, email, access_token) values ("Jablotron", "info@jablotron.cz", "93f39e2f-80de-4033-99ee-249d92736a25");
-- Batman dcb20f8a-5657-4f1b-9f7f-ce65739b359e
insert into Users(name, email, access_token) values ("Batman", "batman@example.com", "dcb20f8a-5657-4f1b-9f7f-ce65739b359e");

create trigger gen_new_uuid before insert on Users for each row set new.access_token = UUID();
create trigger gen_create_date before insert on Endpoints for each row set new.create_date = CURRENT_TIMESTAMP;
create trigger gen_init_check_date before insert on Endpoints for each row set new.last_check_date = CURRENT_TIMESTAMP;
create trigger gen_date before insert on Results for each row set new.date_check = CURRENT_TIMESTAMP;

-- Fix for client node module
use mysql;
update user set authentication_string=NULL, plugin='mysql_native_password' where user='root';
flush privileges;